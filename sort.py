def sort(arr, comp=(lambda i1,i2: i1 < i2)):
    """Tri rapide
        - arr : liste à trier
        - comp : fonction de comparaison
    """
    cpy_arr = list(arr)
    return quicksort(cpy_arr, 0, len(arr) - 1, comp)

def quicksort(arr, first, last, comp):
    """Implémentation du tri rapide
        - arr : liste à trier
        - first : indice de début du tableau
        - last : indice de fin du tableau
        - comp : fonction de comparaison
    """
    if first < last:
        pivot = (first + last) // 2 # on peut choisir un meilleur pivot en fonction de la situation
        arr, pivot = partition(arr, first, last, pivot, comp)
        arr = quicksort(arr, first, pivot - 1, comp)
        arr = quicksort(arr, pivot + 1, last, comp)
    return arr

def partition(arr, first, last, pivot, comp):
    """Partition pour le tri rapide
        - arr : liste à trier
        - first : indice de début du tableau
        - last : indice de fin du tableau
        - pivot : indice de l'élément pivot
        - comp : fonction de comparaison
    """
    arr[pivot], arr[last] = arr[last], arr[pivot]
    j = first
    for i in range(first, last):
        if comp(arr[i], arr[last]): # On compare les rapports entre eux
            arr[i], arr[j] = arr[j], arr[i]
            j += 1
    arr[last], arr[j] = arr[j], arr[last]
    return arr, j