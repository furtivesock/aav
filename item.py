from csv import reader
from csv import writer

"""
    Module qui permet de gérer les objets
"""

class Item:
    """
        Classe qui représente un objet que l'on peut mettre
        dans le sac à dos
    """
    def __init__(self, name, weight, value):
        """Initialisation d'un item
            - name : nom
            - weight : poids
            - value : valeur
        """  
        self.name = name
        self.weight = float(weight)
        self.value = float(value)
        self.rapport = self.value/self.weight

def get_items(filepath):
    """Fonction qui récupère les objets à partir d'un fichier .csv        
        - filepath : chemin du fichier .csv
        Retourne une liste des objets du fichier
    """
    if not filepath.lower().endswith('.csv'):
        raise Exception("Veuillez mettre un fichier .csv en paramètre.")    
    items_list = []
    with open(filepath,newline='') as csv_file:
        csv_input = reader(csv_file, delimiter=';', lineterminator='\n')
        for item in csv_input:
            items_list.append(Item(item[0],item[1],item[2]))
    return items_list

def get_knapsack_items(items_list, filepath_out, knapsack):
    """Génère un fichier .csv à partir d'une liste d'objets donnée
        - items_list : liste non rangée
        - filepath_out : chemin du fichier .csv
        - knapsack : liste des objets dans le sac
    """
    knapsack_names = []
    for item in knapsack:
        knapsack_names.append(item.name)
    with open(filepath_out, 'w') as output:
        csv_output = writer(output, delimiter=';', lineterminator='\n')
        for i in range(len(items_list)):
            is_added = 0
            if items_list[i].name in knapsack_names:
                is_added = 1
            csv_output.writerow([items_list[i].name, is_added])
