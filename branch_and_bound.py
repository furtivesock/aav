
from item import get_items, get_knapsack_items
from sort import sort
from generate_input import generate_input

def get_lower_bound(items_list, max_capacity):
    """ Méthode greedy pour avoir la borne inférieure la plus élevée
        - items_list : liste des objets à mettre dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            borne minimum pour la méthode PSE
    """
    lower_bound = 0
    weight = 0
    i = len(items_list)-1
    while i >= 0 and weight + items_list[i].weight <= max_capacity:
        weight += items_list[i].weight
        lower_bound += items_list[i].value
        i -= 1
    return lower_bound

def get_upper_bound(items_list, begin):
    """ Retourne la borne supérieure à partir de l'index begin
        - items_list : liste des objets à mettre dans le sac
        - begin : index où commencer
        - return :
            borne maximum pour la méthode PSE
    """
    return sum(items_list[i].value for i in range(begin, len(items_list)))

def babe_pure(items_list, max_capacity):
    """ Méthode PSE avec pile
        - items_list : liste des objets à mettre dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            - la valeur trouvée pour la solution
            - le poids total trouvé pour la solution
            - la liste des objets dans le sac             
    """                
    knapsack = []

    # poids et valeur actuels du sac
    bag_weight = bag_value = 0
    
    # solution de valeur et de poids
    solution_value = solution_weight = 0

    # pile de booleens : l'objet a été pris ou pas
    stack = [] 
    
    # Récupération de la borne inférieure pour la valeur
    minimum = get_lower_bound(items_list, max_capacity)

    # on stocke dans un tableau les bornes supérieures
    # pour éviter de les recalculer plusieurs fois
    upper_bounds = [None] * (len(items_list) + 1)

    while True:
        while len(stack) < len(items_list):
            if bag_weight + items_list[len(stack)].weight > max_capacity:
                break
            bag_weight += items_list[len(stack)].weight
            bag_value += items_list[len(stack)].value
            stack.append(True)

        if bag_value > solution_value:
            knapsack = list(stack)
            solution_value = bag_value
            solution_weight = bag_weight 
            minimum = max(minimum, solution_value)

        while len(stack) > 0:
            pris = stack.pop()
            if pris:
                bag_weight -= items_list[len(stack)].weight
                bag_value -= items_list[len(stack)].value
                if upper_bounds[len(stack)] is None:
                    upper_bounds[len(stack)] = get_upper_bound(items_list, len(stack))
                if bag_value + upper_bounds[len(stack)] >= minimum:
                    stack.append(False)
                    break
        
        if len(stack) <= 0:
            break
        
    knapsack = [items_list[i] for i, presence in enumerate(knapsack) if presence]
    return solution_value, solution_weight, knapsack

# PSE
def babe(filepath_in, filepath_out, max_capacity):
    """Récupère dans un fichier .csv les objets, résout le problème de sac à dos
    avec la méthode PSE puis écrit dans un fichier .csv la solution
        - filepath_in : chemin du fichier des objets
        - filepath_out : chemin du fichier des objets mis dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            - la valeur trouvée de la solution
            - le poids total trouvé de la solution
            - la liste des objets dans le sac
    """
    knapsack = []

    # Récupération des objets du fichier
    items_list = get_items(filepath_in)
    items_list_sorted = sort(items_list, (lambda i1,i2: i1.rapport < i2.rapport))


    bag_value, bag_weight, knapsack = babe_pure(items_list_sorted, max_capacity)

    get_knapsack_items(items_list, filepath_out, knapsack)
    return bag_value, bag_weight, len(knapsack)

