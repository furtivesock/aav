from item import get_items, get_knapsack_items

def nb_digits_after_decimal_point(nb):
    """Retourne le nombre de chiffres après la virgule
        - nb : un nombre réel
        - return :
            le nombre de chiffres après la virgule
    """
    nb_str = str(nb)
    ind_point = nb_str.find(".")
    if ind_point == -1:
        return 0
    return (len(nb_str)-1) - ind_point

def dynamic_pure(items_list, max_capacity, prec):
    """Méthode dynamique de résolution du problème du sac à dos
        - items_list : liste des objets à mettre dans le sac
        - max_capacity : capacité maximale du sac
        - prec : précision, nombre maximal de chiffres après la virgule pour le poids
        - return :
            - la valeur trouvée pour la solution
            - le poids total trouvé pour la solution
            - la liste des objets dans le sac 
    """
    knapsack = []

    # Pour simuler des poids entiers à partir
    # de poids réels à l'origine 
    p_prec = 10 ** prec

    # la taille totale de la matrice pour la résolution dynamique
    max_capacity_w_prec = max_capacity * p_prec

    # le nombre d'objet dans le sac
    len_items = len(items_list)
    
    # on initialise la matrice avec uniquement des 0
    matrix = [[0] * (max_capacity_w_prec + 1) for i in range(len_items)]

    # on initialise la première ligne
    for j in range(int(items_list[0].weight * p_prec), max_capacity_w_prec + 1):
        matrix[0][j] = items_list[0].value
    
    # remplissage du reste de la matrice
    for i in range(1, len_items):
        for j in range(max_capacity_w_prec + 1):
            weight = int(items_list[i].weight * p_prec)
            if weight > j:
                matrix[i][j] = matrix[i-1][j]
            else:
                matrix[i][j] = max(
                    matrix[i-1][j],
                    matrix[i-1][j-weight]+items_list[i].value
                )
    
    bag_weight = max_capacity_w_prec

    while bag_weight > 0 and matrix[len_items-1][bag_weight] == matrix[len_items-1][bag_weight-1]:
        bag_weight -= 1

    bag_value = matrix[len_items-1][bag_weight]

    # on récupère les items de la solution un par un
    j = bag_weight
    while j > 0:
        while i > 0 and matrix[i][j] == matrix[i-1][j]:
            i -= 1
        j -= int(items_list[i].weight * p_prec)
        if j >= 0:
            knapsack.append(items_list[i])
        i -= 1

    return bag_value, bag_weight / p_prec, knapsack

def dynamic(filepath_in, filepath_out, max_capacity):
    """Récupère dans un fichier .csv les objets, résout le problème de sac à dos
    avec la méthode dynamique puis écrit dans un fichier .csv la solution
        - filepath_in : chemin du fichier des objets
        - filepath_out : chemin du fichier des objets mis dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            - la valeur trouvée de la solution
            - le poids total trouvé de la solution
            - la liste des objets dans le sac
    """
    # Récupération des objets du fichier
    items_list = get_items(filepath_in)

    # Calcul du nombre maximum de chiffres après la virgule pour 
    # le poids de tous les objets
    prec = max(nb_digits_after_decimal_point(item.weight) for item in items_list) 
    
    bag_value, bag_weight, knapsack = dynamic_pure(items_list, max_capacity, prec)

    get_knapsack_items(items_list, filepath_out, knapsack)

    return bag_value, bag_weight, len(knapsack)

    