from item import get_items, get_knapsack_items
from sort import sort

def greedy_pure(items_list, max_capacity):
    """Méthode gloutonne de résolution du problème de sac à dos (non optimale)
        - items_list : liste des objets à mettre dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            - la valeur trouvée de la solution
            - le poids total trouvé de la solution
            - la liste des objets dans le sac
    """
    current_capacity = 0
    bag_value = 0
    knapsack = []

    # Tri de la liste des objets en fonction de leur rapport
    # dans l'ordre décroissant
    items_list_sorted = sort(items_list, (lambda i1,i2: i1.rapport < i2.rapport))

    i = len(items_list)-1
    while i >= 0 and current_capacity + items_list_sorted[i].weight <= max_capacity:
        knapsack.append(items_list_sorted[i])
        current_capacity += items_list_sorted[i].weight
        bag_value += items_list_sorted[i].value
        i -= 1

    return bag_value, current_capacity, knapsack

def greedy(filepath_in, filepath_out, max_capacity):
    """Récupère dans un fichier .csv les objets, résout le problème de sac à dos
    avec la méthode gloutonne puis écrit dans un fichier .csv la solution
        - filepath_in : chemin du fichier des objets
        - filepath_out : chemin du fichier des objets mis dans le sac
        - max_capacity : capacité maximale du sac
        - return :
            - la valeur trouvée de la solution
            - le poids total trouvé de la solution
            - la liste des objets dans le sac
    """
    items_list = get_items(filepath_in)

    bag_value, bag_weight, knapsack = greedy_pure(items_list, max_capacity)

    # Insertion des résultats dans un fichier
    get_knapsack_items(items_list, filepath_out, knapsack)

    return bag_value, bag_weight, len(knapsack)

